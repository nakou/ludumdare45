﻿public enum SlotType 
{
    EMPTY,
    SMALL_HOUSE,
    BIG_HOUSE,
    SAWMILL,
    FARM,
    TOWNHALL,
    WAGON
}
