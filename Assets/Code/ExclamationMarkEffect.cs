﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExclamationMarkEffect : MonoBehaviour
{
    public Vector3 positionUp;
    public Vector3 positionDown;
    float speed = .3f;
    bool goUp = true;

    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = positionDown;
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;
        if(goUp)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, positionUp, step);
            if (Vector3.Distance(transform.localPosition, positionUp) < 0.01)
            {
                goUp = !goUp;
            }
        }
        else
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, positionDown, step);
            if (Vector3.Distance(transform.localPosition, positionDown) < 0.01)
            {
                goUp = !goUp;
            }
        }
    }
}
