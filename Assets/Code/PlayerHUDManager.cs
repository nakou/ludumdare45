﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerHUDManager : MonoBehaviour
{
    [Header("Resource Panel")]
    public TextMeshProUGUI wood;
    public TextMeshProUGUI food;
    public TextMeshProUGUI money;
    public TextMeshProUGUI happiness;
    public TextMeshProUGUI population;
    public TextMeshProUGUI time;
    public TextMeshProUGUI speed;
    public TextMeshProUGUI woodVariation;
    public TextMeshProUGUI foodVariation;
    public TextMeshProUGUI moneyVariation;

    [Header("Slot Info")]
    public TextMeshProUGUI slotType;
    public TextMeshProUGUI slotTerrainType;
    public TextMeshProUGUI workerCount;
    public GameObject infoSlot;
    [Header("Place Wagon Button")]
    public GameObject placeWagonButton;
    [Header("Build Buttons")]
    public GameObject buildFarmButton;
    public GameObject buildSawmillButton;
    public GameObject buildSmallHouseButton;
    public GameObject upgradeToBigHouseButton;
    public GameObject upgradeToTownhallButton;
    [Header("Management Buttons")]
    public GameObject addWorkerButton;
    public GameObject removeWorkerButton;
    [Header("Alert Popup")]
    public GameObject alertPopup;
    public TextMeshProUGUI alertPopupText;
    public float popupAlert = 5;
    float popupAlertTimer = 0;
    [Header("Event Popup")]
    public GameObject Popup;
    public TextMeshProUGUI PopupText;
    [Header("Help Popup")]
    public GameObject HelpPopup;
    [Header("GameOver Popup")]
    public GameObject GameOverPopup;
    [Header("Technical")]
    public GameManager gameManager;
    public PlayerController playerController;

    public void Start()
    {
        HideInfoBloc();
        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        playerController = GetComponent<PlayerController>();
        alertPopup.SetActive(false);
    }

    public void Update()
    {
        if(playerController.SelectedSlot != null && infoSlot.activeSelf)
        {
            Vector3 positionOnScreen = Camera.main.WorldToScreenPoint(playerController.SelectedSlot.gameObject.transform.position) + new Vector3(0,150,0);
            infoSlot.transform.position = positionOnScreen;
        }
        if (alertPopup.activeSelf)
        {
            if(popupAlertTimer > 0)
            {
                popupAlertTimer -= Time.deltaTime;
            }
            else
            {
                popupAlertTimer = 0;
                alertPopup.SetActive(false);
            }
        }
    }

    public void ShowAlert(string message)
    {
        if (alertPopup.activeSelf)
        {
            alertPopupText.text += "\n" + message;
        }
        else
        {
            alertPopupText.text = message;
        }
        popupAlertTimer = popupAlert;
        alertPopup.SetActive(true);
    }

    public void ShowPopup(string message)
    {
        PopupText.text = message;
        Popup.SetActive(true);
    }

    public void ClosePopup()
    {
        Popup.SetActive(false);
    }

    public void ShowGameOverPopup()
    {
        GameOverPopup.SetActive(true);
    }

    public void CloseGameOverPopup()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowHelpPopup()
    {
        HelpPopup.SetActive(true);
    }

    public void CloseHelpPopup()
    {
        HelpPopup.SetActive(false);
    }

    public void ShowInfoBloc()
    {
        infoSlot.SetActive(true);
        slotType.text = "" + Localization.GetSlotTypeLocale(playerController.SelectedSlot.SlotType);
        slotTerrainType.text = "" + Localization.GetSlotTerrainTypeLocale(playerController.SelectedSlot.SlotTerrainType);
        placeWagonButton.SetActive(false);
        buildFarmButton.SetActive(false);
        buildSawmillButton.SetActive(false);
        buildSmallHouseButton.SetActive(false);
        upgradeToBigHouseButton.SetActive(false);
        upgradeToTownhallButton.SetActive(false);
        addWorkerButton.SetActive(false);
        removeWorkerButton.SetActive(false);
        workerCount.gameObject.SetActive(false);
        if (gameManager.mapManager.isOnConstructibleGround(playerController.SelectedSlot))
        {
            if (!gameManager.hasWagonBeenPlaced)
            {
                placeWagonButton.SetActive(true);
                return;
            }
            ShowConstructionButtons();
        }
        if(playerController.SelectedSlot.SlotTerrainType == SlotTerrainType.OCCUPIED && playerController.SelectedSlot.ConstructionTime <= 0)
        {
            ShowUpgradeButtons();
            ShowManagementButtons();
            UpdateHUDWorkers(playerController.SelectedSlot);
        }
    }

    public void HideInfoBloc()
    {
        infoSlot.SetActive(false);
    }

    public void ShowConstructionButtons()
    {
        if (gameManager.mapManager.isFarmConstructibleHere(playerController.SelectedSlot))
        {
            buildFarmButton.SetActive(true);
        }
        if (gameManager.mapManager.isSawmillConstructibleHere(playerController.SelectedSlot))
        {
            buildSawmillButton.SetActive(true);
        }
        buildSmallHouseButton.SetActive(true);
    }

    public void ShowUpgradeButtons()
    {
        if(playerController.SelectedSlot.SlotType == SlotType.SMALL_HOUSE)
        {
            upgradeToBigHouseButton.SetActive(true);
        }
        if (playerController.SelectedSlot.SlotType == SlotType.WAGON)
        {
            upgradeToTownhallButton.SetActive(true);
        }
    }

    public void ShowManagementButtons()
    {
        if(playerController.SelectedSlot.SlotType == SlotType.FARM ||
            playerController.SelectedSlot.SlotType == SlotType.SAWMILL ||
            playerController.SelectedSlot.SlotType == SlotType.TOWNHALL)
        {
            workerCount.gameObject.SetActive(true);
            addWorkerButton.SetActive(true);
            removeWorkerButton.SetActive(true);
        }
    }

    public void UpdateHUDWorkers(Slot slot)
    {
        workerCount.text = "Workers \n (" + slot.Worker + "/" + slot.workerNeeded + ")";
    }

    public void SpeedUpTime()
    {
        gameManager.Speed += 1;
    }

    public void SlowDownTime()
    {
        gameManager.Speed -= 1;
    }

    public void AddWorker()
    {
        gameManager.AddWorker(playerController.SelectedSlot);
    }

    public void RemoveWorker()
    {
        gameManager.RemoveWorker(playerController.SelectedSlot);
    }

    public void PlaceWagon()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.WAGON);
        HideInfoBloc();
    }

    public void ConstructFarm()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.FARM);
        HideInfoBloc();
    }

    public void ConstructSmallHome()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.SMALL_HOUSE);
        HideInfoBloc();
    }

    public void ConstructSawmill()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.SAWMILL);
        HideInfoBloc();
    }

    public void UpgradeToBigHouse()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.BIG_HOUSE);
        HideInfoBloc();
    }

    public void UpgradeToTownHall()
    {
        gameManager.Construction(playerController.SelectedSlot, SlotType.TOWNHALL);
        HideInfoBloc();
    }
}
