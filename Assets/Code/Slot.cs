﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    public GameObject slotSelector;
    [Header("Installations")]
    public GameObject smallHouse;
    public GameObject bigHouse;
    public GameObject sawmill;
    public GameObject farm;
    public GameObject wagon;
    public GameObject townhall;
    public GameObject underConstruction;
    public GameObject understaffed;
    [Header("Terrain")]
    public GameObject water;
    public GameObject grass;
    public GameObject rocks;
    public GameObject forest;

    public int xCoord;
    public int zCoord;

    public float constructionTime;
    public float ConstructionTime {
        get { return constructionTime; }
        set
        {
            constructionTime = value;
            underConstruction.SetActive(constructionTime > 0);
            if(constructionTime <= 0)
            {
                TriggerHUDRefresh();
                Worker = 0;
            }
        }
    }

    public int workerNeeded = 0;

    public int worker;
    public int Worker
    {
        get { return worker; }
        set
        {
            worker = value;
            understaffed.SetActive(worker < workerNeeded);
        }
    }

    public SlotType slotType;
    public SlotType SlotType {
        get { return slotType; }
        set {
            slotType = value;
            ShowSlotType(value);
        }
    }
    public SlotTerrainType slotTerrainType;
    public SlotTerrainType SlotTerrainType
    {
        get { return slotTerrainType; }
        set
        {
            slotTerrainType = value;
            ShowSlotTerrainType(value);
        }
    }

    public bool selected;
    public bool Selected
    {
        get { return selected; }
        set
        {
            selected = value;
            slotSelector.SetActive(value);
        }
    }

    GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(ConstructionTime > 0)
        {
            ConstructionTime -= Time.deltaTime * gameManager.Speed;
        }
    }

    void ShowSlotType(SlotType type)
    {
        underConstruction.SetActive(true);
        understaffed.SetActive(false);
        smallHouse.SetActive(false);
        bigHouse.SetActive(false);
        townhall.SetActive(false);
        sawmill.SetActive(false);
        farm.SetActive(false);
        switch (type)
        {
            case (SlotType.EMPTY):
                underConstruction.SetActive(false);
                break;
            case (SlotType.WAGON):
                wagon.SetActive(true);
                break;
            case (SlotType.BIG_HOUSE):
                bigHouse.SetActive(true);
                break;
            case (SlotType.SMALL_HOUSE):
                smallHouse.SetActive(true);
                break;
            case (SlotType.TOWNHALL):
                townhall.SetActive(true);
                break;
            case (SlotType.FARM):
                farm.SetActive(true);
                break;
            case (SlotType.SAWMILL):
                sawmill.SetActive(true);
                break;
            default:
                break;
        }
    }

    void ShowSlotTerrainType(SlotTerrainType type)
    {
        water.SetActive(false);
        grass.SetActive(false);
        rocks.SetActive(false);
        forest.SetActive(false);
        switch (type)
        {
            case (SlotTerrainType.GRASS):
                grass.SetActive(true);
                break;
            case (SlotTerrainType.WATER):
                water.SetActive(true);
                break;
            case (SlotTerrainType.ROCKS):
                rocks.SetActive(true);
                break;
            case (SlotTerrainType.FOREST):
                forest.SetActive(true);
                break;
            case (SlotTerrainType.OCCUPIED):
                break;
            default:
                break;
        }
    }

    public bool IsUnderConstruction
    {
        get { return constructionTime > 0; }
    }

    public bool IsNotManed
    {
        get { return worker == 0; }
    }

    public void TriggerHUDRefresh()
    {
        gameManager.CalculatePopAndVarsAndHappinessLive();
    }
}
