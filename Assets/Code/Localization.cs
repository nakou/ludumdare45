﻿public class Localization
{
    public static string GetSlotTypeLocale(SlotType type)
    {
        switch (type)
        {
            case (SlotType.EMPTY):
                return "---";
            case (SlotType.BIG_HOUSE):
                return "Big house \n +5$ | -3 Wood \n +6 happiness";
            case (SlotType.SMALL_HOUSE):
                return "Small house \n +2$ | -1 Wood \n +3 happiness";
            case (SlotType.TOWNHALL):
                return "Townhall \n +10 happiness";
            case (SlotType.FARM):
                return "Farm \n +2 food \n -1$ \n -1 happiness";
            case (SlotType.SAWMILL):
                return "Sawmill \n +6 Wood \n -2 happiness";
        }
        return "";
    }

    public static string GetSlotTerrainTypeLocale(SlotTerrainType type)
    {
        switch (type)
        {
            case (SlotTerrainType.GRASS):
                return "Water";
            case (SlotTerrainType.WATER):
                return "Water";
            case (SlotTerrainType.ROCKS):
                return "Rocks";
            case (SlotTerrainType.FOREST):
                return "Forest";
        }
        return "";
    }
}
