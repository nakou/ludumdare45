﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = .1f;

    float horizontal = 0;
    float vertical = 0;
    bool click = false;
    bool escape = false;

    public Slot selectedSlot;

    public Slot SelectedSlot {
        get { return selectedSlot; }
        set
        {
            if (selectedSlot != null)
                selectedSlot.Selected = false;
            if(value != null)
            {
                selectedSlot = value;
                selectedSlot.Selected = true;
                playerHUDManager.ShowInfoBloc();
            }
            else
            {
                selectedSlot = value;
                playerHUDManager.HideInfoBloc();
            }
        }
    }
    GameManager gameManager;
    PlayerHUDManager playerHUDManager;
    public EventSystem eventSystem;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        playerHUDManager = GetComponent<PlayerHUDManager>();
        selectedSlot = null;
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        click = Input.GetButtonDown("Fire1");
        escape = Input.GetButtonDown("Cancel");
        if (click)
        {
            SelectSlot();
        }
        Move();
        if (Input.GetButtonDown("Cancel"))
        {
            DeselectEverything();
        }
    }

    void SelectSlot()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
        {
            Slot slot = hitInfo.transform.gameObject.GetComponent<Slot>();
            if (slot != null)
            {
                if(SelectedSlot == slot)
                {
                    SelectedSlot = null;
                    return;
                }
                SelectedSlot = slot;
            }
        }
    }

    void DeselectEverything()
    {
        if(SelectedSlot != null)
        {
            SelectedSlot = null;
        }

    }

    void Move()
    {
        transform.Translate(new Vector3(0, 0, vertical * moveSpeed));
        transform.Translate(new Vector3(horizontal * moveSpeed, 0 , 0));
    }
}
