﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Map mapManager;
    public PlayerHUDManager playerHUDManager;

    public int wood = 0;
    public int food = 0;
    public int money = 0;
    public int happiness = 0;
    public int population = 0; // Population total
    public int shelter = 0; // Shelter places
    public int freePopulation = 0; //Population minus workers

    int woodVariation = 0;
    int moneyVariation = 0;
    int foodVariation = 0;

    float timer = 0;
    int minutePassed = 0;

    public int speed = 1;
    public int Speed
    {
        get { return speed; }
        set
        {
            if(value < 5 && value > 0)
            {
                speed = value;
            }
        }
    }

    public List<Slot> installationsList;
    public bool hasWagonBeenPlaced = false;

    // Start is called before the first frame update
    void Start()
    {
        mapManager = GameObject.FindGameObjectWithTag("Map Manager").GetComponent<Map>();
        playerHUDManager = GameObject.FindGameObjectWithTag("HUD Manager").GetComponent<PlayerHUDManager>();
        installationsList = new List<Slot>();
        playerHUDManager.ShowPopup("Welcome to \"We started with nothing\"! " +
            "\n The goal is to build a stable city. " +
            "\n Start by placing the first wagon of your people. " +
            "\n Be sure to have wood and food, and to have fun! " +
            "\n See ya in the endgame " +
            "\n - Captain Nakou");
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasWagonBeenPlaced)
        {
            return;
        }
        if(timer < 60 )
        {
            timer += Time.deltaTime * speed;
        }
        else
        {
            playerHUDManager.ShowAlert("Another month as passed!");
            timer = 0;
            minutePassed++;
            CalculateNewValues();
            UpdateHUD();
        }
        UpdateTimeAndPopAndVarsAndHappiness();
    }

    void UpdateHUD()
    {
        playerHUDManager.wood.text = "" + wood;
        playerHUDManager.food.text = "" + food;
        playerHUDManager.money.text = "" + money;
    }

    void UpdateTimeAndPopAndVarsAndHappiness()
    {
        int timerToInt = (int)(timer/2);
        playerHUDManager.time.text = "" + timerToInt + " Day - Month " + minutePassed + "";
        playerHUDManager.speed.text = "x" + Speed;
        playerHUDManager.population.text = "" + population + "/" + freePopulation + "/" + shelter;
        playerHUDManager.happiness.text = "" + happiness;
        playerHUDManager.woodVariation.text = "(" + woodVariation + ")";
        playerHUDManager.foodVariation.text = "(" + foodVariation + ")";
        playerHUDManager.moneyVariation.text = "(" + moneyVariation + ")";
    }

    public void CalculatePopAndVarsAndHappinessLive()
    {
        int shelter = 0;
        int happiness = 0;
        woodVariation = 0;
        moneyVariation = 0;
        foodVariation = -population;
        foreach (Slot slot in installationsList)
        {
            if (slot.IsUnderConstruction)
            {
                continue;
            }
            switch (slot.SlotType)
            {
                case SlotType.FARM:
                    if (slot.IsNotManed)
                        continue;
                    foodVariation += 2;
                    moneyVariation -= 1;
                    happiness -= 1;
                    break;
                case SlotType.SAWMILL:
                    if (slot.IsNotManed)
                        continue;
                    happiness -= 2;
                    woodVariation += 6;
                    break;
                case SlotType.SMALL_HOUSE:
                    happiness += 3;
                    shelter += 3;
                    woodVariation -= 1;
                    moneyVariation += 2;
                    break;
                case SlotType.BIG_HOUSE:
                    woodVariation -= 3;
                    moneyVariation += 5;
                    happiness += 6;
                    shelter += 6;
                    break;
                case SlotType.WAGON:
                    break;
                case SlotType.TOWNHALL:
                    if (slot.IsNotManed)
                        continue;
                    happiness += 10;
                    break;
            }
        }
        this.happiness = happiness;
        this.shelter = shelter;
    }

    void CalculateNewValues()
    {
        CalculatePopAndVarsAndHappinessLive();
        int deadPeople = 0;
        int tempDeadPerson = 0;
        int foodConsumed = population;
        food -= foodConsumed;
        foreach (Slot slot in installationsList)
        {
            if (slot.IsUnderConstruction)
            {
                continue;
            }
            tempDeadPerson = 0;
            switch (slot.SlotType)
            {
                case SlotType.FARM:
                    if (slot.IsNotManed)
                        continue;
                    food += 2;
                    money -= 1;
                    tempDeadPerson = (Random.Range(0, 100) < 5) ? 1 : 0;
                    if(tempDeadPerson > 0)
                    {
                        playerHUDManager.ShowAlert("A person died on your farm!");
                    }
                    break;
                case SlotType.SAWMILL:
                    if (slot.IsNotManed)
                        continue;
                    wood += 6;
                    deadPeople += (Random.Range(0, 100) < 15) ? 1 : 0;
                    if (tempDeadPerson > 0)
                    {
                        playerHUDManager.ShowAlert("A person died on your sawmill!");
                    }
                    break;
                case SlotType.SMALL_HOUSE:
                    wood -= 1;
                    money += 2;
                    break;
                case SlotType.BIG_HOUSE:
                    wood -= 3;
                    money += 5;
                    break;
                case SlotType.WAGON:
                    break;
                case SlotType.TOWNHALL:
                    if (slot.IsNotManed)
                        continue;
                    break;
            }
            if(slot.Worker > 0)
            {
                slot.Worker -= tempDeadPerson;
                population -= tempDeadPerson;
            }
        }
        if(food < 0)
        {
            food = 0;
            playerHUDManager.ShowAlert("Your population needs more food!");
        }
        UpdatePopulation(happiness / 2);
    }

    public void UpdatePopulation(int populationHappinessVar)
    {
        if(populationHappinessVar == 0)
        {
            playerHUDManager.ShowAlert("Nobody joined your settlement. Try to make your people more happy!");
            return;
        }
        // First, apply the popModifier from all pop variables
        // if popModifier is positive, the new pop is added to free and general population if there is enough shelter
        if(population > shelter)
        {
            playerHUDManager.ShowAlert("There were not enough shelter for everyone, and " + (shelter - population) + " people left the settlement...");
            populationHappinessVar = (shelter - population);
        }
        if(populationHappinessVar > 0)
        {
            int totalNewPop = population + populationHappinessVar;
            if (totalNewPop <= shelter)
            {
                freePopulation += populationHappinessVar;
                population += populationHappinessVar;
            }
            else
            {
                int overPopulation = totalNewPop - shelter;
                int finalNewPopPossible = populationHappinessVar - overPopulation;
                freePopulation += finalNewPopPossible;
                population += finalNewPopPossible;
                if(finalNewPopPossible > 0)
                {
                    playerHUDManager.ShowAlert("There were not enough shelter for everyone, and only" + finalNewPopPossible + " people joined the settlement...");
                }
            }
            return;
        }
        else
        {
            // if popModifier is negative, we delete it from free population.
            freePopulation += populationHappinessVar;
            if(freePopulation < 0)
            {
                // if free population now negative, means we have to delete people from occupied population
                // we delete enough worker in random installation
                // we update population values with results from those calculations
                int popToDelete = Mathf.Abs(freePopulation);
                Slot slotToDeleteWorker = null;
                int recursiveSecurity = 0;
                for (int i = 0; i < popToDelete; i++)
                {
                    slotToDeleteWorker = findSlotWithWorkerActive(slotToDeleteWorker, recursiveSecurity);
                    Debug.Log("Took " + recursiveSecurity + " tries.");
                    slotToDeleteWorker.Worker -= 1;
                }
                population += populationHappinessVar;
                freePopulation = population;
                if(population <= 0)
                {
                    playerHUDManager.ShowGameOverPopup();
                }
            }
            return;
        }
    }

    Slot findSlotWithWorkerActive(Slot slotToDeleteWorker, int recursiveSecurity)
    {
        slotToDeleteWorker = installationsList[Random.Range(0, installationsList.Count)];
        if (slotToDeleteWorker.Worker > 0)
        {
            return slotToDeleteWorker;
        }
        else
        {
            recursiveSecurity++;
            if(recursiveSecurity > 100)
            {
                Debug.LogError("Can't find anything... Better bugged than crashed!");
                return null;
            } 
            else
            {
                return findSlotWithWorkerActive(slotToDeleteWorker, recursiveSecurity);
            }
        }
    }

    public void AddWorker(Slot slot)
    {
        if(freePopulation - 1 >= 0)
        {
            if(slot.Worker < slot.workerNeeded)
            {
                slot.Worker += 1;
                freePopulation -= 1;
            }
            else
            {
                playerHUDManager.ShowAlert("You already have enough workers here!");
            }
        }
        else
        {
            playerHUDManager.ShowAlert("You dont have enough free population...");
        }
        UpdateHUDWorkers(slot);
        CalculatePopAndVarsAndHappinessLive();
    }

    void UpdateHUDWorkers(Slot slot)
    {
        playerHUDManager.UpdateHUDWorkers(slot); 
    }

    public void RemoveWorker(Slot slot)
    {
        if(slot.Worker > 0)
        {
            slot.Worker -= 1;
            freePopulation += 1;
        }
        else
        {
            playerHUDManager.ShowAlert("You have nobody working here...");
        }
        UpdateHUDWorkers(slot);
        CalculatePopAndVarsAndHappinessLive();
    }

    public void AddInstallation(Slot slot)
    {
        installationsList.Add(slot);
        CalculatePopAndVarsAndHappinessLive();
    }

    public void Construction(Slot slot, SlotType type)
    {
        string result = null;
        bool upgrade = false;
        switch (type)
        {
            case SlotType.FARM:
                result = tryToBuild(slot, SlotType.FARM, 5, 0,0,0,10,1);
                break;
            case SlotType.SAWMILL:
                result = tryToBuild(slot, SlotType.SAWMILL, 25, 0, 0, 0, 25, 1);
                break;
            case SlotType.SMALL_HOUSE:
                result = tryToBuild(slot, SlotType.SMALL_HOUSE, 30, 0, 0, 0, 20, 0);
                break;
            case SlotType.BIG_HOUSE:
                result = tryToBuild(slot, SlotType.BIG_HOUSE, 60, 0, 0, 0, 40, 0);
                upgrade = true;
                break;
            case SlotType.WAGON:
                slot.ConstructionTime += 1;
                slot.workerNeeded = 0;
                wood += 100;
                food += 15;
                money += 10;
                population += 3;
                freePopulation += 3;
                /* CHEATER!
                wood += 10000;
                food += 1500;
                money += 1000;
                population += 300;
                freePopulation += 300;
                */
                hasWagonBeenPlaced = true;
                result = null;
                slot.SlotType = type;
                slot.SlotTerrainType = SlotTerrainType.OCCUPIED;
                break;
            case SlotType.TOWNHALL:
                result = tryToBuild(slot, SlotType.TOWNHALL, 150, 50, 0, 100, 40, 1);
                upgrade = true;
                break;
        }
        if(result != null) {
            playerHUDManager.ShowAlert(result);
            return;
        }
        UpdateHUD();
        if (upgrade)
        {
            if (type == SlotType.TOWNHALL)
            {
                playerHUDManager.ShowPopup("Yay! " +
                    "\n You've constructed a Townhall. That's now a real city. " +
                    "\n Congratulation, this is basically the end of the game. " +
                    "\n Hope you liked it! " +
                    "\n Happy Ludum Dare 45! " +
                    "\n - Captain Nakou");
            }
            return;
        }
        AddInstallation(slot);
    }

    string materialsChecker(int woodNeeded, int moneyNeeded, int foodNeeded, int populationNeeded)
    {
        if(woodNeeded > wood)
        {
            return "You need more wood.";
        }
        if(moneyNeeded > money)
        {
            return "You need more money.";
        }
        if (foodNeeded > food)
        {
            return "You need more food.";
        }
        if (populationNeeded > population)
        {
            return "You need more people.";
        }
        return null;
    }

    string tryToBuild(Slot slot, SlotType type, int woodNeeded, int moneyNeeded, int foodNeeded, int populationNeeded, int constructionTime, int workersNeeded)
    {
        string retVal = materialsChecker(woodNeeded, moneyNeeded,foodNeeded,populationNeeded);
        if(retVal == null)
        {
            slot.constructionTime += constructionTime;
            slot.workerNeeded = workersNeeded;
            wood -= woodNeeded;
            money -= moneyNeeded;
            food -= foodNeeded;
            slot.SlotType = type;
            slot.SlotTerrainType = SlotTerrainType.OCCUPIED;
        }
        return retVal;
    }
}
