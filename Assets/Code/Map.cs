﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{

    [Header("Da Map")]
    public List<List<Slot>> coordMap;
    public List<Slot> mapSlotList;
    public int size;

    [Header("Technical")]
    GameManager gameManager;
    MapGenerator mapGenerator;

    void Start()
    {
        mapGenerator = GetComponent<MapGenerator>();
        gameManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        size = mapGenerator.size;
        mapGenerator.mapInfos = this;
        mapGenerator.InitMap();
    }

    public void DeselectAll()
    {
        foreach(Slot s in mapSlotList)
        {
            s.Selected = false;
        }
    }

    public bool isOnConstructibleGround(Slot slot)
    {
        return slot.SlotTerrainType == SlotTerrainType.GRASS;
    }

    public bool isFarmConstructibleHere(Slot slot)
    {
        if(slot.SlotTerrainType != SlotTerrainType.GRASS)
        {
            return false;
        }
        List<Slot> checkForFarm = checkableSlots(slot);
        foreach(Slot s in checkForFarm)
        {
            if(s.SlotType == SlotType.FARM || s.SlotTerrainType == SlotTerrainType.WATER)
            {
                return true;
            }
        }
        return false;
    }

    public bool isSawmillConstructibleHere(Slot slot)
    {
        if (slot.SlotTerrainType != SlotTerrainType.GRASS)
        {
            return false;
        }
        List<Slot> checkForFarm = checkableSlots(slot);
        foreach (Slot s in checkForFarm)
        {
            if (s.SlotTerrainType == SlotTerrainType.FOREST)
            {
                return true;
            }
        }
        return false;
    }

    public List<Slot> checkableSlots(Slot slot)
    {
        List<Slot> nearSlotList = new List<Slot>();
        int x = slot.xCoord;
        int z = slot.zCoord;
        if(x + 1 < size)
        {
            nearSlotList.Add(coordMap[x + 1][z]);
        }
        if (x - 1 >= 0)
        {
            nearSlotList.Add(coordMap[x - 1][z]);
        }
        if (z + 1 < size)
        {
            nearSlotList.Add(coordMap[x][z + 1]);
        }
        if (z - 1 >= 0)
        {
            nearSlotList.Add(coordMap[x][z - 1]);
        }
        return nearSlotList;
    }

}
