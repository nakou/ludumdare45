﻿public enum SlotTerrainType
{
    GRASS,
    WATER,
    ROCKS,
    FOREST,
    OCCUPIED
}
