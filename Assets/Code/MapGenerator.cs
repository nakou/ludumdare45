﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [Header("Map Params")]
    public int size = 20;
    public int chanceToGetWater = 5;
    public int chanceToGetRocks = 10;
    public int chanceToGetForest = 25;
    public bool randomTerrain = true;
    public bool randomInstallations = false;

    [Header("Prefabs")]
    public GameObject slot;

    [Header("Technical")]
    public Map mapInfos;
    public GameManager gameManager;

    //##### Randomizer Infos ######
    bool hasSpawnTownHallOrWagon = false;

    public void InitMap()
    {
        GameObject newSlotGameObject;
        Slot newSlot;
        Vector3 newSlotPosition;
        Quaternion newSlotRotation;
        mapInfos.mapSlotList = new List<Slot>();
        mapInfos.coordMap = new List<List<Slot>>();
        List<Slot> newSlotList;
        for (int x = 0; x < size; x++)
        {
            newSlotList = new List<Slot>();
            for (int z = 0; z < size; z++)
            {
                newSlotPosition = new Vector3(x, 0, z);
                newSlotRotation = Quaternion.identity;
                newSlotGameObject = Instantiate(slot, newSlotPosition, newSlotRotation);
                newSlot = newSlotGameObject.GetComponent<Slot>();
                InitSlot(newSlot, x, z);
                mapInfos.mapSlotList.Add(newSlot);
                newSlotList.Add(newSlot);
            }
            mapInfos.coordMap.Add(newSlotList);
        }
        CreateActualRiver();
        EnlargeWater();
    }

    void InitSlot(Slot slot, int x, int z)
    {
        if (randomTerrain)
        {
            slot.SlotType = SlotType.EMPTY;
            slot.SlotTerrainType = GetRandomTerrainType();
        }
        if (randomInstallations)
        {
            if (slot.SlotTerrainType == SlotTerrainType.GRASS)
            {
                slot.SlotTerrainType = SlotTerrainType.OCCUPIED;
                slot.SlotType = GetRandomType();
            }
        }
        slot.Selected = false;
        slot.xCoord = x;
        slot.zCoord = z;
        return;
    }

    void EnlargeWater()
    {
        int rand;
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                if(mapInfos.coordMap[x][z].SlotTerrainType == SlotTerrainType.GRASS)
                {
                    if (hasWaterAdjacent(x, z))
                    {
                        rand = Random.Range(0, 10);
                        if(rand > 7)
                        {
                            mapInfos.coordMap[x][z].SlotTerrainType = SlotTerrainType.WATER;
                        }
                    }
                }
            }
        }
    }

    void CreateActualRiver()
    {
        int zStartPoint = Random.Range(0, size);
        FlowWater(0, zStartPoint);
    }

    void FlowWater(int x, int z)
    {
        bool xPlusExist;
        bool xMinusExist;
        bool zPlusExist;
        bool zMinusExist;
        bool goNorth;
        bool goSouth;
        bool goWest;
        while (x < size - 1)
        {
            xPlusExist = x < size - 1;
            xMinusExist = x > 0;
            zPlusExist = z < size - 1;
            zMinusExist = z > 0;
            goNorth = (zPlusExist && (Random.Range(0, 100) <= 15));
            goSouth = (zMinusExist && (Random.Range(0, 100) <= 15));
            goWest = (xMinusExist && (Random.Range(0, 100) <= 5));
            if (goWest)
                x -= 1;
            else if (goSouth)
                z -= 1;
            else if (goNorth)
                z += 1;
            else
                x += 1;
            mapInfos.coordMap[x][z].SlotTerrainType = SlotTerrainType.WATER;
        }

    }

    bool hasWaterAdjacent(int x, int z)
    {
        bool xPlusExist = x < size - 1;
        bool xMinusExist = x > 0;
        bool zPlusExist = z < size - 1;
        bool zMinusExist = z > 0;
        if(xPlusExist)
        {
            if(mapInfos.coordMap[x+1][z].SlotTerrainType == SlotTerrainType.WATER)
            {
                return true;
            }
        }
        if (xMinusExist)
        {
            if (mapInfos.coordMap[x - 1][z].SlotTerrainType == SlotTerrainType.WATER)
            {
                return true;
            }
        }
        if (zPlusExist)
        {
            if (mapInfos.coordMap[x][z + 1].SlotTerrainType == SlotTerrainType.WATER)
            {
                return true;
            }
        }
        if (zMinusExist)
        {
            if (mapInfos.coordMap[x][z - 1].SlotTerrainType == SlotTerrainType.WATER)
            {
                return true;
            }
        }
        return false;
    }

    void CreateForest()
    {

    }

    SlotType GetRandomType()
    {
        int i = Random.Range(0, 40);
        switch (i)
        {
            case 1:
                return SlotType.BIG_HOUSE;
            case 2:
                return SlotType.SMALL_HOUSE;
            case 3:
                return SlotType.FARM;
            case 4:
                if (hasSpawnTownHallOrWagon)
                {
                    return SlotType.EMPTY;
                }
                hasSpawnTownHallOrWagon = true;
                return SlotType.TOWNHALL;
            case 5:
                return SlotType.SAWMILL;
            case 6:
                if (hasSpawnTownHallOrWagon)
                {
                    return SlotType.EMPTY;
                }
                hasSpawnTownHallOrWagon = true;
                return SlotType.WAGON;
        }
        return SlotType.EMPTY;
    }

    SlotTerrainType GetRandomTerrainType()
    {
        int rollToGetRocks = Random.Range(0, 100);
        int rollToGetWater = Random.Range(0, 100);
        int rollToGetForest = Random.Range(0, 100);
        if (rollToGetForest < chanceToGetForest)
        {
            return SlotTerrainType.FOREST;
        }
        if (rollToGetWater < chanceToGetWater)
        {
            return SlotTerrainType.WATER;
        }
        if (rollToGetRocks < chanceToGetRocks)
        {
            return SlotTerrainType.ROCKS;
        }
        return SlotTerrainType.GRASS;
    }
}
