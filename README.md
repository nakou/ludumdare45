# Ludum Dare 45

This is my entry to the LD45 JAM.
It's a constuction/management game (unconsciously) inspired by Kingdoms and Castles

## Rules 

- 1 people consume 1 food/min
- Happiness is linked to people comming/leaving (1p * happiness/min)
- people can be assign to ressources/installation


#  UTILITY                


## Small house

Can be constructed anywhere

Cost :
- 30 wood

Provide :
- Shelter for 2 people
- 2$ / min
- -+3 happiness
- -1 wood/min

## Big house

Can be upgraded from a small house.

Cost :
- 60 wood

Provide :
- Shelter for 6 people
- 5$ / min
- +6 happiness
- -2 wood/min


## Wagon

Starting building

Cost :
None

Provide :
- Single paiement of 10$, 100 wood, 15 food
- 3 population


## Townhall
Can be upgraded from a wagon.

Cost :

- 150 wood
- 50$

Provide :

- +10 happiness


### PRODUCTION

## Sawmill
Cab be build anywere if in proximity of a forest

Cost :
- 25 wood
- 5$

Provide :

- +6 wood/min
- -2 happiness
- -Chance to kill every minute : 10%


## Farm 
Can be constructed in proximity of water or another farm
Cost :
- 5 wood to construct
- 1$

Provide :

- 2 food / min
- -1 happiness
- Chance to kill every minute : 5%
